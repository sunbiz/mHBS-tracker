package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001%B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\f\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\f\u001a\u0004\b\u0014\u0010\u0015R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001b\u0010\f\u001a\u0004\b\u0019\u0010\u001aR\u001b\u0010\u001c\u001a\u00020\u001d8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b \u0010\f\u001a\u0004\b\u001e\u0010\u001f\u00a8\u0006&"}, d2 = {"Ldhis2/org/analytics/charts/data/Chart;", "", "chartType", "Ldhis2/org/analytics/charts/data/ChartType;", "graphData", "Ldhis2/org/analytics/charts/data/Graph;", "(Ldhis2/org/analytics/charts/data/ChartType;Ldhis2/org/analytics/charts/data/Graph;)V", "graphToBarChartMapper", "Ldhis2/org/analytics/charts/mappers/GraphToBarChart;", "getGraphToBarChartMapper", "()Ldhis2/org/analytics/charts/mappers/GraphToBarChart;", "graphToBarChartMapper$delegate", "Lkotlin/Lazy;", "graphToLineChartMapper", "Ldhis2/org/analytics/charts/mappers/GraphToLineChart;", "getGraphToLineChartMapper", "()Ldhis2/org/analytics/charts/mappers/GraphToLineChart;", "graphToLineChartMapper$delegate", "graphToNutritionChartMapper", "Ldhis2/org/analytics/charts/mappers/GraphToNutritionChart;", "getGraphToNutritionChartMapper", "()Ldhis2/org/analytics/charts/mappers/GraphToNutritionChart;", "graphToNutritionChartMapper$delegate", "graphToTableMapper", "Ldhis2/org/analytics/charts/mappers/GraphToTable;", "getGraphToTableMapper", "()Ldhis2/org/analytics/charts/mappers/GraphToTable;", "graphToTableMapper$delegate", "graphToValueMapper", "Ldhis2/org/analytics/charts/mappers/GraphToValue;", "getGraphToValueMapper", "()Ldhis2/org/analytics/charts/mappers/GraphToValue;", "graphToValueMapper$delegate", "getChartView", "Landroid/view/View;", "context", "Landroid/content/Context;", "ChartBuilder", "dhis_android_analytics_dhisDebug"})
public final class Chart {
    private final kotlin.Lazy graphToLineChartMapper$delegate = null;
    private final kotlin.Lazy graphToNutritionChartMapper$delegate = null;
    private final kotlin.Lazy graphToBarChartMapper$delegate = null;
    private final kotlin.Lazy graphToTableMapper$delegate = null;
    private final kotlin.Lazy graphToValueMapper$delegate = null;
    private final dhis2.org.analytics.charts.data.ChartType chartType = null;
    private final dhis2.org.analytics.charts.data.Graph graphData = null;
    
    private final dhis2.org.analytics.charts.mappers.GraphToLineChart getGraphToLineChartMapper() {
        return null;
    }
    
    private final dhis2.org.analytics.charts.mappers.GraphToNutritionChart getGraphToNutritionChartMapper() {
        return null;
    }
    
    private final dhis2.org.analytics.charts.mappers.GraphToBarChart getGraphToBarChartMapper() {
        return null;
    }
    
    private final dhis2.org.analytics.charts.mappers.GraphToTable getGraphToTableMapper() {
        return null;
    }
    
    private final dhis2.org.analytics.charts.mappers.GraphToValue getGraphToValueMapper() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getChartView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    private Chart(dhis2.org.analytics.charts.data.ChartType chartType, dhis2.org.analytics.charts.data.Graph graphData) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Ldhis2/org/analytics/charts/data/Chart$ChartBuilder;", "", "()V", "chartType", "Ldhis2/org/analytics/charts/data/ChartType;", "graphData", "Ldhis2/org/analytics/charts/data/Graph;", "build", "Ldhis2/org/analytics/charts/data/Chart;", "withGraphData", "graph", "withType", "dhis_android_analytics_dhisDebug"})
    public static final class ChartBuilder {
        private dhis2.org.analytics.charts.data.ChartType chartType;
        private dhis2.org.analytics.charts.data.Graph graphData;
        
        @org.jetbrains.annotations.NotNull()
        public final dhis2.org.analytics.charts.data.Chart.ChartBuilder withType(@org.jetbrains.annotations.NotNull()
        dhis2.org.analytics.charts.data.ChartType chartType) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final dhis2.org.analytics.charts.data.Chart.ChartBuilder withGraphData(@org.jetbrains.annotations.NotNull()
        dhis2.org.analytics.charts.data.Graph graph) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final dhis2.org.analytics.charts.data.Chart build() {
            return null;
        }
        
        public ChartBuilder() {
            super();
        }
    }
}