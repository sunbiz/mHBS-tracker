package dhis2.org.analytics.charts.providers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005H\u0016J\u000e\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Ldhis2/org/analytics/charts/providers/NutritionColorsProviderImpl;", "Ldhis2/org/analytics/charts/providers/NutritionColorsProvider;", "()V", "nutritionColors", "", "", "getColorAt", "position", "dhis_android_analytics_dhisDebug"})
public final class NutritionColorsProviderImpl implements dhis2.org.analytics.charts.providers.NutritionColorsProvider {
    private final java.util.List<java.lang.Integer> nutritionColors = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.util.List<java.lang.Integer> nutritionColors() {
        return null;
    }
    
    @java.lang.Override()
    public int getColorAt(int position) {
        return 0;
    }
    
    public NutritionColorsProviderImpl() {
        super();
    }
}